
# Sample Rails app for Toolforge Build Service

The app is based on the official [Getting started with Rails](https://guides.rubyonrails.org/getting_started.html) guide.
You can see it [deployed on Toolforge](https://sample-ruby-rails-buildpack-app.toolforge.org/).
For a detailed guide on how to deploy a Rails application on Toolforge, please refer to [this tutorial on Wikitech](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service/My_first_Buildpack_Ruby_on_Rails_tool)

For simplicity, this app uses a sqlite database.
In production, you would want to configure and use ToolsDB.
Remember to change the settings in `config/database.yml`
